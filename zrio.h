#ifndef __ZRIO_RJ
#define __ZRIO_RJ

#include <stdio.h>
#include <sys/types.h>
#include "zutil.h"
#include "inftrees.h"
#include "inflate.h"
#include "zlib.h"

#define WINSIZE 32768       /* sliding window size */
#define CHUNK 16384         /* file input buffer size */

typedef struct point {
	int64_t out;
	int64_t in;
	int bits;
	unsigned char *window;
} Point;

typedef struct access {
    int have;           /* number of list entries filled in */
    int size;           /* number of list entries allocated */
    Point *list; /* allocated list */
} Access;

typedef struct zr_stream {
	FILE *acc_file;
	Access *access;
	int64_t last_in, in, out;
	unsigned char window[WINSIZE];
	int64_t pos;
	int fid;
	int z_eof;
	int z_err;
	void *inbuf;
	void *outbuf;
	z_stream *stream;
} zr_stream;

zr_stream* zropen(const char *filename);

int zrmkindex(const char *filename, int level,
	void (*notify)(void *obj, unsigned char *buf, int buf_size, int64_t pos), void *obj);

int64_t zrread(void *buf, int64_t len, zr_stream *stream);

/** 
 * More efficient than zrseek + zrread
 */
int64_t zrgets(void *buf, int64_t len, int64_t offset, zr_stream *stream);

int zreof(zr_stream *stream);
int zrerror(zr_stream *stream);

int64_t zrtell(zr_stream *stream);

int64_t zrseek(zr_stream *stream, int64_t offset);

int zrskip(zr_stream *stream, int bytes);

void zrclose(zr_stream *stream);

#endif
