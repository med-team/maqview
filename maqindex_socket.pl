#!/usr/bin/perl -w

use strict;
use warnings;
use MaqIndex;

my $mi = MaqIndex->new(-port=>1768);
#print $mi->{_n_ref}, "\n";
#for my $ref (keys %{$mi->{_refs}}) {
#  print join("\t", $ref, @{$mi->{_refs}{$ref}}), "\n";
#}
my @bufs;
$mi->get("1:100-110", \@bufs);
$mi->mapview(\@bufs);
$mi->get("2:100-110", \@bufs);
$mi->mapview(\@bufs);
$mi->destroy;
