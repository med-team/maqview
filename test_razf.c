#include "razf.h"
#include "kstream.h"
#include <time.h>

KSTREAM_INIT(RAZF*, razf_read, 4096)

int main(int argc, char **argv){
	RAZF *rz = razf_open_r(open(argv[1], O_RDONLY));
	unsigned int n = 32 * 1024 * 1024;
	void *buffer = alloca(1<<12);
	clock_t t1 = clock();
	int i;
	for(i=0;i<n;i++){
		razf_read(rz, buffer, 1);
	}
	clock_t t2 = clock();
	fprintf(stderr, " -- time %.3lf , n = %u in %s -- %s:%d --\n", ((double)(t2-t1) / CLOCKS_PER_SEC), n, __FUNCTION__, __FILE__, __LINE__);
	razf_seek(rz, 0, SEEK_SET);
	t1 = clock();
	for(i=0;i<n>>12;i++){
		razf_read(rz, buffer, 1<<12);
	}
	t2 = clock();
	fprintf(stderr, " -- time %.3lf , n = %u in %s -- %s:%d --\n", ((double)(t2-t1) / CLOCKS_PER_SEC), n, __FUNCTION__, __FILE__, __LINE__);
	razf_seek(rz, 0, SEEK_SET);
	kstream_t *ks = ks_init(rz);
	t1 = clock();
	for(i=0;i<n;i++){
		ks_getc(ks);
	}
	t2 = clock();
	fprintf(stderr, " -- time %.3lf , n = %u in %s -- %s:%d --\n", ((double)(t2-t1) / CLOCKS_PER_SEC), n, __FUNCTION__, __FILE__, __LINE__);
	razf_close(rz);
	return 0;
}
