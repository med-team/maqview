#ifndef __MAQMAP_INDEX_RJ
#define __MAQMAP_INDEX_RJ

#include "maqmap.h"
#include "zrio.h"
#include "btree.h"

#define maplet maqmap1_t

#define read_pos(p) ((int64_t)((p)>>1))

#define read_strand(p) (p&1)

typedef struct {
	maqmap_t *mm;
	BTree **trees;
	zr_stream *stream;
	int head_size;
} MapIndex;

MapIndex* load_map_index(const char *filename, int auto_make_index_if_no);

MapIndex* make_map_index(const char *filename, int level);

int iter_map_index(MapIndex *mi, int ref_id, int64_t start, int64_t end, int (*handle_maqmap1_t)(void *obj, maplet *let), void *obj);

maplet* read_map_index(MapIndex *mi, int ref_id, int64_t start, int64_t end, int *n_let);

maplet* read_map_next_to(MapIndex *mi, int ref_id, int64_t pos, int *n_let);

void close_map_index(MapIndex *mi);

void num2str(int64_t number, char *string);

int64_t str2num(char *string);

#endif
