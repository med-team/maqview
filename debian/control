Source: maqview
Maintainer: Debian-Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Andreas Tille <tille@debian.org>,
           Charles Plessy <plessy@debian.org>,
           Pranav Ballaney <ballaneypranav@gmail.com>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13),
               freeglut3-dev,
               libxi-dev
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/med-team/maqview
Vcs-Git: https://salsa.debian.org/med-team/maqview.git
Homepage: https://maq.sourceforge.net/maqview.shtml
Rules-Requires-Root: no

Package: maqview
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends}
Description: graphical read alignment viewer for short gene sequences
 Maqview is graphical read alignment viewer. It is specifically designed
 for the Maq alignment file and allows you to see the mismatches, base
 qualities and mapping qualities. Maqview is nothing fancy as Consed or
 GAP, but just a simple viewer for you to see what happens in a
 particular region.
 .
 In comparison to tgap-maq, the text-based read alignment viewer written
 by James Bonfield, Maqview is faster and takes up much less memory and
 disk space in indexing. This is possibly because tgap aims to be a
 general-purpose viewer but Maqview fully makes use of the fact that a
 Maq alignment file has already been sorted. Maqview is also efficient in
 viewing and provides a command-line tool to quickly retrieve any region
 in an Maq alignment file.
