#ifndef __BTREE_MAQVIEW_RJ
#define __BTREE_MAQVIEW_RJ

#include <stdio.h>
#include <sys/types.h>
#include "zlib.h"

typedef struct BTree {
	struct BTree *parent;
	struct BTree **childs;
	int64_t left, right;
	int64_t indexs[2];
	int size;
} BTree;

#ifndef BTREE_MAX_ELEMENTS
#define BTREE_MAX_ELEMENTS 1024
#endif

#ifndef BTREE_MAX_NODES
#define BTREE_MAX_NODES 32
#endif

BTree* btree_init();

BTree* btree_bulid(int (*get_next)(void *obj, int64_t last_index, int64_t *value), void *obj);

BTree* btree_append(BTree **tree_ref, BTree *last_node, int *layer, int64_t index, int64_t value);

BTree* btree_find(BTree *tree, int64_t value, int64_t *left_index, int64_t *right_index);

int btree_dump(FILE *out, BTree *tree);

int btree_dump_gz(gzFile out, BTree *tree);

BTree* btree_load(FILE *in);

BTree* btree_load_gz(gzFile in);

void btree_free(BTree *tree);

int64_t _get_min_index(BTree *tree);

int64_t _get_max_index(BTree *tree);

#endif
