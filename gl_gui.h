#ifndef __GL_LIST_RJ
#define __GL_LIST_RJ

#include "maqview.h"

typedef struct GContainer {
	int win_id;
	ViewPanel *parent;
	int visible;
	float win_x, win_y;
	float win_width, win_height;
	float border_size;
	float bgColor[3];
	float textColor[3];
	float borderColor[3];
	float highlightTextColor[3];
	float highlightBgColor[3];
} GContainer;

typedef void (*VALUE_CHANGED)(void *obj, int new_value);

typedef struct GList {
	GContainer window;
	float cell_width, cell_height;
	int id;
	int selected;
	int disp_offset;
	int disp_size;
	int size;
	int capacity;
	char **options;
	VALUE_CHANGED valueChanged;
} GList;


GList* createGList(int id, ViewPanel *vp, float x, float y, float cell_width, float cell_height, int n_disp);

void GList_valueChangedFunc(GList *list, VALUE_CHANGED *valueChanged);

int GList_addOption(GList *list, char *option);

char* GList_getOption(GList *list, int index);

void freeGList(GList *list);

#endif
